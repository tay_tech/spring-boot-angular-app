package com.taytech.ecommerce;

import com.taytech.ecommerce.model.Product;
import com.taytech.ecommerce.service.ProductService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.math.BigDecimal;

@SpringBootApplication
public class EcommerceApplication {

	public static void main(String[] args) {
		SpringApplication.run(EcommerceApplication.class, args);
	}

	@Bean
	CommandLineRunner runner(ProductService productService) {
		return args -> {
			productService.save(new Product(1L, "TV Set", BigDecimal.valueOf(300.00), "http://placehold.it/200x100", "Brand new!  52"));
			productService.save(new Product(2L, "Game Console", BigDecimal.valueOf(200.00), "http://placehold.it/200x100", "Latest Updates"));
			productService.save(new Product(3L, "Sofa", BigDecimal.valueOf(100.00), "http://placehold.it/200x100", "Four Seater"));
			productService.save(new Product(4L, "Icecream", BigDecimal.valueOf(5.00), "http://placehold.it/200x100", "1 Gallon"));
			productService.save(new Product(5L, "Beer", BigDecimal.valueOf(3.00), "http://placehold.it/200x100", "Buy Local"));
			productService.save(new Product(6L, "Phone", BigDecimal.valueOf(500.00), "http://placehold.it/200x100", "New Camera"));
			productService.save(new Product(7L, "Watch", BigDecimal.valueOf(30.00), "http://placehold.it/200x100", "Great Deal"));
		};
	}

}
