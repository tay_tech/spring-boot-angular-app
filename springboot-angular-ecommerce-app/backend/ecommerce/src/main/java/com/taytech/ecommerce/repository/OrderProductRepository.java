package com.taytech.ecommerce.repository;

import com.taytech.ecommerce.model.OrderProduct;
import com.taytech.ecommerce.model.OrderProductPK;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderProductRepository extends JpaRepository<OrderProduct, OrderProductPK> {
}
