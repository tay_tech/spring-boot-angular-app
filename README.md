# Spring Boot Angular E-Commerce Application

## Overview
This application serves as practice for me to learn more about microservices,
develop a backend that communicates with the frontend, and create a CI/CD pipeline.


## Prerequisites
* Java 8+
* Basic knowledge of Spring Boot
* An IDE ([Intellij IDEA](https://www.jetbrains.com/idea/), [Eclipse](https://www.eclipse.org/downloads/), [VS Code](https://code.visualstudio.com/))


## Technologies Used
* Spring Boot 2.5.3
* Java 11
* Angular 12
* Gradle 7.1.1
* Project Lombok
* Spock Testing Framework
* Swagger


## Install Project
* Run `git clone https://gitlab.com/tay_tech/spring-boot-angular-app.git` or download a [zip](https://gitlab.com/tay_tech/spring-boot-angular-app/-/archive/main/spring-boot-angular-app-main.zip) of the project

## How to Run
* Backend- Start the project by running `./gradlew bootrun` in the command line or run it from your IDE
* Frontend - Start the project by running `ng serve` and navigate to http://localhost:4200/
* Swagger- Run the project and navigate to `http://localhost:8081/v2/api-docs`

## References
* [Spring Boot](https://spring.io/microservices)
* [Angular](https://angular.io/)
* [Gradle](https://docs.gradle.org/7.1.1/userguide/userguide.html)
* [Spock](https://spockframework.org/)
* [Project Lombok](https://projectlombok.org/features/all)
* [Swagger](https://swagger.io/specification/v2/)
* [Setting Up Swagger 2 with a Spring REST API](https://www.baeldung.com/swagger-2-documentation-for-spring-rest-api)